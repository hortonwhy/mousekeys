#include <linux/uinput.h>
//#include <string.h>
//#include <fcntl.h>
#include "fdsetup.h"

void emit(int fd, int type, int code, int val)
{
   struct input_event ie;

   ie.type = type;
   ie.code = code;
   ie.value = val;
   /* timestamp values below are ignored */
   ie.time.tv_sec = 0;


   ie.time.tv_usec = 0;

   write(fd, &ie, sizeof(ie));
}

/* emit function is identical to of the first example */

int main(void)
{
  int i = 50;
  int fd = fdsetup(); // return file descriptor for virtual device

   /* Move the mouse diagonally, 5 units per axis */
   while (i--) {
      emit(fd, EV_REL, REL_X, 5);
      emit(fd, EV_REL, REL_Y, 5);
      emit(fd, EV_SYN, SYN_REPORT, 0);
      usleep(15000);
   }

   /*
    * Give userspace some time to read the events before we destroy the
    * device with UI_DEV_DESTOY.
    */
   sleep(1);

   ioctl(fd, UI_DEV_DESTROY);
   close(fd);

   return 0;
}
